'use strict';

pokemonApp.component('pokemonDetail', {

  controller: function PokemonDetailCtrl($routeParams, PokemonsService) {

    this.pokemonLoaded = false;
    //console.log(11);
    var self = this;

    this.pokemon = PokemonsService.get({
      pokemonId: $routeParams['pokemonId']
    }, (successResult) => {
      //console.log(self);
      this.pokemonLoaded = true;
      this.notfoundError = false;

      this.activeTab = 1;
      this.disableControlTab = true;

    }, function (errorResult) {
      self.pokemonLoaded = true;
      self.notfoundError = true;
    });

    this.deletePokemon = function(pokemonId) {

      this.pokemon.$delete({
        pokemonId: pokemonId
      }, function(successResult) {
        // Окей!
        self.deletionSuccess = true;
      }, errorResult => {
        // Не окей..
        this.deletionError = true;
      });

    }
  },

  templateUrl: './src/PokemonDetail/PokemonDetailComp.html'
});
