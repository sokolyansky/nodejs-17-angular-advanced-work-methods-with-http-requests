'use strict';

pokemonApp.component('pokemonList', {

    controller: function PokemonListCtrl(PokemonsService) {

        this.pokemons = PokemonsService.query({},
        function (response) {
            //console.log(response);
        },
        function (error) {
            console.log(error.message);
        });
        /*this.pokemons.then(function (response) {
            return console.log(response);
        });*/
        //console.log(this.pokemons);
    },

    templateUrl: './src/PokemonList/PokemonList.html'

});
